FROM ubuntu:20.04
LABEL maintainer="jonathanpiron@gmail.com"

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        curl \
        dnsutils \
        iproute2 \
        iputils-ping \
        ldnsutils \
        lsof \
        mtr \
        net-tools \
        netcat \
        sockstat \
        strace \
        telnet \
        vim

ENTRYPOINT ["bash"]
