DOCKER_VERSION := latest
DOCKER_OPTS := $(shell [ "${NO_CACHE}" = "1" ] && echo '--no-cache')
DOCKER_OPTS += $(shell [ "${PULL}" = "1" ] && echo '--pull')
REGISTRY_IMAGE ?= jpiron/tools

.PHONY: all
all: build publish

.PHONY: build
build:
	@echo -e '\n### BUILD ###'
	docker build ${DOCKER_OPTS} \
		-f Dockerfile \
		-t $(REGISTRY_IMAGE):$(DOCKER_VERSION) .

.PHONY: publish
publish: docker-login
	@echo -e '\n### PUBLISH ###'
	docker push $(REGISTRY_IMAGE):$(DOCKER_VERSION)

.PHONY: clean
clean:
	@echo -e '\n### CLEAN ###'
	docker images --filter "reference=$(REGISTRY_IMAGE):$(DOCKER_VERSION)" --quiet 2>/dev/null | xargs --no-run-if-empty docker rmi --force

.PHONY: purge
purge: clean
	@echo -e '\n### PURGE ###'
	docker images --filter "reference=$(REGISTRY_IMAGE)" --quiet 2>/dev/null | xargs --no-run-if-empty docker rmi --force

.PHONY: docker-login
docker-login: check-docker-env
	@echo -e '\n### DOCKER LOGIN ###'
	@docker login -u '$(DOCKER_REGISTRY_USER)' -p '$(DOCKER_REGISTRY_PASSWORD)' 2> /dev/null

.PHONY: check-docker-env
check-docker-env:
ifndef DOCKER_REGISTRY_USER
	$(error DOCKER_REGISTRY_USER is undefined)
endif
ifndef DOCKER_REGISTRY_PASSWORD
	$(error DOCKER_REGISTRY_PASSWORD is undefined)
endif
